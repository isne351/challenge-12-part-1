#include <iostream>
using namespace std;

template<class T>

void swap_values(T& variable1, T& variable2) {
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}

template<class T>

int index_of_smallest(T& a, int start_index, int number_used) {
	int min = a[start_index];
	int index_of_min = start_index;

	for (int index = start_index + 1; index < number_used; index++) {

		if (a[index] < min) {
			min = a[index];
			index_of_min = index;
		}
	}

	return index_of_min;
}

template<class T>

void sort(T& a, int number_used) {
	int index_of_next_smallest;
	for (int index = 0; index < number_used - 1; index++) {
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}


int main() {


	int int_demo[6] = { 1,9,3,4,5,6 };

	double double_demo[6] = { 5,9,4,7,10,6 };

	char char_demo[6] = { 'd','z','c','b','a' };

	sort(int_demo, 6);

	cout << " Int short : ";

	for (int i = 0; i < 6; i++) {

		cout << int_demo[i] << " ";

	}
	sort(double_demo, 6);

	cout << endl << " Double short : ";

	for (int i = 0; i < 6; i++) {

		cout << double_demo[i] << " ";

	}

	sort(char_demo, 6);

	cout << endl <<" Char short : ";

	for (int i = 0; i < 6; i++) {

		cout << char_demo[i] << " ";

	}

	cout << endl;

	system("pause");

}